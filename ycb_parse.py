import os
import os.path as Path
import glob
import json
import scipy.io
from scipy.spatial.transform import Rotation as R
from dotenv import load_dotenv

load_dotenv(Path.join(Path.dirname(__file__), 'globals.env'))
DATASET_DIR = os.getenv('DATASET_DIR')
DRILL_INDEX = int(os.getenv('DRILL_INDEX'))

# Load JSON base
json_annot = {
    "objects": [{
        "bounding_box": {
            "top_left": [],
            "bottom_right": []
        }
    }]
}

# Convert each mat file to json
mat_filenames = glob.glob1(DATASET_DIR, "*.mat")
txt_filenames = glob.glob1(DATASET_DIR, "*.txt")
for mat_filename in mat_filenames:
    # Read mat
    mat_path = os.path.join(DATASET_DIR, mat_filename)
    mat = scipy.io.loadmat(mat_path)

    # Find object index
    objects_idxs = [itm[0] for itm in mat["cls_indexes"]]
    drill_idx = objects_idxs.index(DRILL_INDEX)

    # Parse bbox from txt file
    txt_path = mat_path.replace("meta.mat", "box.txt")
    with open(txt_path) as txt_data:
        line = txt_data.readlines()[drill_idx].strip("\n")
        [xmin, ymin, xmax, ymax] = [float(val) for val in line.split(" ")[1:5]]
    json_annot["objects"][0]["bounding_box"]["top_left"] = [xmin, ymin]
    json_annot["objects"][0]["bounding_box"]["bottom_right"] = [xmax, ymax]

    # Convert transform matrix into xyz translation + xyzw rotation quaternion
    transf_matrix = mat['poses'][:, :, drill_idx]
    translation = transf_matrix[:, 3]
    json_annot["objects"][0]["location"] = translation.tolist()
    rot_matrix = transf_matrix[0:3, 0:3]
    json_annot["objects"][0]["quaternion_xyzw"] = R.from_matrix(
        rot_matrix).as_quat().tolist()

    # Write annotations as JSON
    filename = mat_path.replace("meta.mat", "meta.json")
    with open(filename, 'w') as f:
        json.dump(json_annot, f)
