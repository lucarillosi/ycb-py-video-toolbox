```
mat["poses"][:,:,0]
array([[-0.24266832, -0.96942131, -0.03653016, -0.108319  ],
       [-0.45749141,  0.1475636 , -0.87688441,  0.04842776],
       [ 0.8554613 , -0.19607956, -0.47931142,  0.66642644]])

0.66642644 è la distanza (scoperto da codice matlab)


import numpy as np
from scipy.spatial.transform import Rotation as R

ndds_m = np.array([
    [-0.80070000886917114, 0.2046000063419342, 0.56300002336502075],
    [0.012900000438094139, -0.93370002508163452, 0.35769999027252197],
    [-0.59890002012252808, -0.29370000958442688, -0.74500000476837158]
])
ndds_q_xyzw = R.from_matrix(ndds_m).as_quat()


ycb_m = np.array([[-0.24266832, -0.96942131, -0.03653016],
                  [-0.45749141,  0.1475636, -0.87688441],
                  [0.8554613, -0.19607956, -0.47931142]])
ycb_q_xyzw = R.from_matrix(ycb_m).as_quat()
pass
```
