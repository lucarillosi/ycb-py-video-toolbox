import matplotlib.pyplot as plt
from numpy.lib.npyio import save
from bbox import BBox
from PIL import Image
import os.path as Path
import os
from dotenv import load_dotenv
import json
from typing import Optional, List
import numpy as np
import open3d as o3d
import glob

load_dotenv(Path.join(Path.dirname(__file__), 'globals.env'))


def _plot_bbox_over_img(img: Image, bbox: BBox, save_path: Optional[str]):
    # Plot img
    plt.clf()
    ax = plt.subplot(1, 1, 1)
    ax.set_title('Bounding box')
    ax.imshow(img)
    rect = bbox.get_matplotlib_patch()
    ax.add_patch(rect)
    plt.ion()
    plt.show()
    plt.pause(0.001)

    if save_path is not None:
        plt.savefig(save_path)


def _plot_depth(rgb: Image, dpt: Image, centroid_gt: Optional[List[float]],
                quat_xyzw_gt: Optional[List[float]],
                save_path: Optional[str]) -> None:
    (w, h) = rgb.size
    geometries = []

    # Prepare images
    rgb = o3d.geometry.Image(np.array(rgb))
    dpt = o3d.geometry.Image(np.array(dpt, dtype=np.uint16))
    rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(
        rgb,
        dpt,
        depth_scale=10000.0,
        depth_trunc=9999999,
        convert_rgb_to_intensity=False)

    # Set up camera
    intrinsic = np.array([[1.066778e+03, 0.000000e+00, 3.129869e+02],
                          [0.000000e+00, 1.067487e+03, 2.413109e+02],
                          [0.000000e+00, 0.000000e+00, 1.000000e+00]])
    pinhole_camera = o3d.camera.PinholeCameraIntrinsic(width=w,
                                                       height=h,
                                                       fx=256,
                                                       fy=256,
                                                       cx=w / 2,
                                                       cy=h / 2)
    pinhole_camera.intrinsic_matrix = intrinsic

    # Display point cloud
    pcl = o3d.geometry.PointCloud.create_from_rgbd_image(rgbd, pinhole_camera)
    geometries.append(pcl)

    # Display ground truth
    obj_pose_gt = o3d.geometry.TriangleMesh.create_coordinate_frame(
        size=0.1, origin=centroid_gt)
    obj_pose_gt.rotate(
        o3d.geometry.get_rotation_matrix_from_quaternion(
            np.roll(quat_xyzw_gt, 1)))  # quaternion in wxyz format!!
    geometries.append(obj_pose_gt)

    # Run Open3d
    vis = o3d.visualization.Visualizer()
    vis.create_window(window_name="Open3D", width=w, height=h, visible=False)
    for geo in geometries:
        vis.add_geometry(geo)
    view = vis.get_view_control()
    view.rotate(50, 1100)

    # Save snapshot
    if save_path is not None:
        vis.capture_screen_image(save_path, True)


def _plot_annotations(sample_id: str):
    print("Plotting annotations for " + sample_id)
    # Read RGB
    rgb_path = Path.join(os.getenv('DATASET_DIR'), sample_id + "-color.png")
    rgb = Image.open(rgb_path)
    (w, h) = rgb.size

    # Read JSON annotations
    ann_path = Path.join(os.getenv('DATASET_DIR'), sample_id + "-meta.json")
    with open(ann_path) as json_data:
        ann = json.load(json_data)
        tl = ann["objects"][0]["bounding_box"]["top_left"]
        br = ann["objects"][0]["bounding_box"]["bottom_right"]
        bb = tl + br
        centroid = ann["objects"][0]["location"]
        quaternion = ann["objects"][0]["quaternion_xyzw"]
    bbox = BBox(bb, import_format="classic", img_width=w, img_height=h)

    # Plot BBox
    save_path = Path.join(os.getenv('DATASET_DIR'), sample_id + "-show.png")
    _plot_bbox_over_img(rgb, bbox, save_path=save_path)

    # Read depth
    dpt_path = Path.join(os.getenv('DATASET_DIR'), sample_id + "-depth.png")
    dpt = Image.open(dpt_path)
    save_path = save_path.replace("-show.png", "-render.png")
    _plot_depth(rgb, dpt, centroid, quaternion, save_path)


def _plot_all_annotations():
    files = glob.glob1(os.getenv('DATASET_DIR'), "*-meta.json")
    for file in files:
        _plot_annotations(file.strip("-meta.json"))


_plot_all_annotations()
